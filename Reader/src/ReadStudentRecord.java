import com.google.gson.Gson;

import java.io.*;

/**
 * Created by sumnima on 9/10/2016.
 */
public class ReadStudentRecord implements Runnable {
    public String lastname, firstname, id, dateofBirth,ln,fn,ids;
    String file;
    OutputStreamWriter write=null;
    Gson gson = new Gson();

    ReadStudentRecord(String f, OutputStreamWriter w)
    {

        this.file=f;
        this.write=w;
    }
    @Override
    public void run() {

        BufferedReader br;
        String zero ="0";
        try {
            br = new BufferedReader(new FileReader(file));
            try {
                String x="";
                int i=0,j=0,k=0,l=0,count=0,m=0,p=0;
                while ((x = br.readLine()) != null) {
                    i=i+1;
                    count=count +1;
                    if(!x.equals(zero))
                    {
                            if(i==1 || i==k+4)
                            {
                                this.ln=x;
                                k=i;
                            }
                            if(i==2 || i==j+4)
                            {
                                j=i;
                                this.fn=x;
                            }
                            if(i==3 || i==p+4)
                            {
                                p=i;
                                this.ids=x;
                                }
                            if(i==4 || i==l+4)
                            {
                                l=i;
                                this.dateofBirth=x;
                                lastname=ln;
                                firstname=fn;
                                id=ids;
                                StudentRecord r = new StudentRecord.StudentRecordBuilder(lastname,   firstname).id(id).dateOfBirth(dateofBirth).build();
                                String json = gson.toJson(r);
                                write.write(json.toString()+"\n");
                                System.out.println("StudentCreated"+json.toString()+"\n");
                            }
                    }
                    else
                    {
                        if(m==0)
                        {
                            ids=x;
                            m=m+1;
                        }
                    }
                }
                br.close();
                write.flush();
                write.close();
            } catch (FileNotFoundException e) {
                System.out.println(e);
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}