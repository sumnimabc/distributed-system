import java.io.IOException;
import java.net.Socket;

/**
 * Created by sumnima on 11/7/2016.
 */
public class Communication {
    int portNumber;
    String hostName;
    Socket socket = null;

    public Communication( String hostName, int portNumber) {
        this.portNumber = portNumber;
        this.hostName = hostName;
        try {
            socket = new Socket(hostName, portNumber);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
