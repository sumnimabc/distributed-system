/**
 * Created by sumnima on 9/10/2016.
 */

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class StudentRecord implements Serializable{

    String lastName;    // Required
    String firstName;   // Required
    String id;          // Optional
    String dateOfBirth; // Optional
    int ages;            // Optional

    public static class StudentRecordBuilder {
        private String firstName;
        private String lastName;
        private String id = "";
        private String dateOfBirth = "";
        private int age = 0;

        public StudentRecordBuilder(String lastName, String firstName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public StudentRecordBuilder id(String s) {
            this.id = s;
            return this;
        }

        public StudentRecordBuilder dateOfBirth(String s){
            this.dateOfBirth = s;
            return this;
        }

        public StudentRecordBuilder age(int age){
            this.age = age;
            return this;
        }
        public String getDateOfBirth() {
            return dateOfBirth;
        }






        public StudentRecord build() {
            return new StudentRecord(this);
        }

    }

    public String getDateOfBirth(){return  this.dateOfBirth;}
    public String getId() {
        return id;
    }
    public  void calcAge() {
        //System.out.println(dateOfBirth);
        if (!dateOfBirth.equals(null)) {
            try {
                Calendar birth = new GregorianCalendar();
                Calendar today = new GregorianCalendar();

                int factor = 0; //to correctly calculate age when birthday has not been yet celebrated
                Date birthDate = new SimpleDateFormat("MM-dd-yyyy").parse(dateOfBirth);
                Date currentDate = new Date(); //current date

                birth.setTime(birthDate);
                today.setTime(currentDate);

                // check if birthday has been celebrated this year
                if (today.get(Calendar.DAY_OF_YEAR) < birth.get(Calendar.DAY_OF_YEAR)) {
                    factor = -1; //birthday not celebrated
                }
                ages = today.get(Calendar.YEAR) - birth.get(Calendar.YEAR) + factor;

            } catch (ParseException e) {
                System.out.println("Given date: " + dateOfBirth + " not in expected format (Please enter a MM-DD-YYYY date)");
            }


        }
    }

    private StudentRecord(StudentRecordBuilder b) {
        this.id = b.id;
        this.lastName = b.lastName;
        this.firstName = b.firstName;
        this.dateOfBirth = b.dateOfBirth;
        this.ages = b.age;
    }

    @Override
    public String toString() {
        return "StudentRecord{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", id='" + id + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", age=" + ages +
                '}';
    }
}
