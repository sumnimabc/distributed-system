/**
 * Created by sumnima on 11/10/2016.
 */
public class Arguments {
    //ClientMainArgs
    int argsLength;
    String arg1;
    String arg2;
    String arg3;

    //When there is no args
    int portNumber;
    String hostName;
    String fileName;
    public static final String HN = "cs4727a.etsu.edu";
    public static final String FN = "./resources/student.txt";
    public static final  int PN = 2001;

    public Arguments(int argsLength, String arg1, String arg2, String arg3) {
        this.argsLength=argsLength;
        this.arg1=arg1;
        this.arg2=arg2;
        this.arg3=arg3;
    }

    public void arguments() {
        if (argsLength == 0) {
            portNumber = PN;
            hostName = HN;
            fileName = FN;
        } else {
            fileName = arg1;
            hostName = arg2;
            portNumber = Integer.parseInt(arg3);
        }
    }
}
