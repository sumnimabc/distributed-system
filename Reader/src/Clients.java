import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * Created by sumnima on 10/3/2016.
 */
public class Clients {
    public static void main(String args[]){

        Socket clientSocket;
        OutputStreamWriter outputStreamWriter;

        //System.out.println(System.getProperty("user.dir"));
        //Arguments- Default and Command Line
        int argsLength=args.length ;
        Arguments arguments = new Arguments(argsLength,args[0], args[1], args[2]);
        arguments.arguments();

        Communication c = new Communication(arguments.hostName,arguments.portNumber);
        System.out.println("Clients started on port"+arguments.portNumber);

        try {
           clientSocket= c.socket;
            outputStreamWriter=new OutputStreamWriter(clientSocket.getOutputStream());
            new Thread(new ReadStudentRecord(arguments.fileName, outputStreamWriter)).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
