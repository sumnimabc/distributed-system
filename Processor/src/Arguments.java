/**
 * Created by sumnima on 11/10/2016.
 */
public class Arguments {
    public static final String HN="localhost";//"cs4727a.etsu.edu";

     //Server
    public static final int SPN=2001;
    public static final String FN= ".resources/writeStudentRecord.txt";
    public static final int MAXSPN=3000;

    //Client
    public static final int CPN=2500;
}
