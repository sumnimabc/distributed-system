import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by sumnima on 11/7/2016.
 */
public class ServerCommunication {
    Socket clientSocket = null;
    ServerSocket ss=null;
    int portNumber;
    String pn;
    String hostName,fileName;
    InputStreamReader r;
    Arguments arguments;
    public ServerCommunication(int portNumber) {
        this.portNumber=portNumber;
        while(true) {
            try {
                ss = new ServerSocket(portNumber);
                break;

                } catch (IOException e) {
                    portNumber = portNumber + 1;
                    if (portNumber == arguments.MAXSPN) {
                        System.out.println("Port not available");
                        return;
                    }
                }
            }
    }

    public Socket accept()
    {
        try {
            clientSocket = ss.accept();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return clientSocket;
    }
}
