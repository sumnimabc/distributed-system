import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.concurrent.BlockingQueue;

/**
 * Created by sumnima on 9/10/2016.
 */
class AgeCalculationWQThread implements Runnable {

    private BlockingQueue<StudentRecord> calcqueue;
    ObjectOutputStream writer;
    StudentRecord sr;

    AgeCalculationWQThread(BlockingQueue<StudentRecord> b, ObjectOutputStream writer) {
        this.writer=writer;
        calcqueue = b;
    }

    @Override
    public void run() {
        while (true) {
            try {
                sr = calcqueue.take();
                System.out.println(sr);
                sr.calcAge();
                writer.writeObject(sr);
                System.out.println("\n" + "AgeCalculation+sr"+ sr + "\n");
                if (sr.getId().equals("0")) {
                    break;
                }
            }  catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
