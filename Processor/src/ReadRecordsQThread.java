import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.*;
import java.util.concurrent.BlockingQueue;

/**
 * Created by sumnima on 11/9/2016.
 */
public class ReadRecordsQThread implements Runnable {

    private BlockingQueue<StudentRecord> calcqueue;
    InputStreamReader inputStreamReader;
    StudentRecord sr;
    Gson gson = new Gson();

    ReadRecordsQThread(InputStreamReader reader, BlockingQueue<StudentRecord> b) {
        this.inputStreamReader=reader;
        calcqueue = b;
    }

    @Override
    public void run() {
        JsonReader jReader;
        String x = "";
        BufferedReader br = new BufferedReader(inputStreamReader);
        try {
            while (true) {
                        x=br.readLine();
                        jReader = new JsonReader(new StringReader(x));
                        jReader.setLenient(true);
                        System.out.println("reader\n" + "X" + x);
                        sr = gson.fromJson(jReader, StudentRecord.class);
                        System.out.println("sr\n" + sr);
                        sr.calcAge();
                        calcqueue.offer(sr);
                        if (sr.getId().equals("0")) {
                            break;
                        }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}