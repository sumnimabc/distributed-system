import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by sumnima on 11/9/2016.
 */
public class ProcessorMiddleware {

    public static void main(String args[])
    {
        Arguments arguments = new Arguments();
        ExecutorService executor = Executors.newFixedThreadPool(10);
        BlockingQueue<StudentRecord> bq = new ArrayBlockingQueue<StudentRecord>(1024);

        //Socket Clients/Server
        Socket clientSocket;
        Socket clientS;
        InputStreamReader read;
        ObjectOutputStream objectOutputStream;

        System.out.println("Server Started");
        ServerCommunication sc = new ServerCommunication(arguments.SPN);
        System.out.println("Server is using"+arguments.SPN);
        clientSocket= sc.accept();

        //Client
        System.out.println("Client Started");
        ClientCommunication cc= new ClientCommunication(arguments.HN,arguments.CPN);
        System.out.println("Client is using"+arguments.CPN);

        try {
            //server
            read=new InputStreamReader(clientSocket.getInputStream());

            //client
            clientS=cc.socket;
            objectOutputStream=new ObjectOutputStream(clientS.getOutputStream());
            Runnable readRecordsQThread = new Thread(new ReadRecordsQThread(read, bq));
            Runnable ageCalculationWQThread = new AgeCalculationWQThread(bq,objectOutputStream);

            // 5 thread pool
            executor.execute(readRecordsQThread);
            executor.execute(ageCalculationWQThread);
            //executor.shutdown();
            //while (!executor.isTerminated()) {
            //}
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
