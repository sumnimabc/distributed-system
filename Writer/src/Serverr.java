import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by sumnima on 10/3/2016.
 */
public class Serverr {

    public Serverr() {
    }
    public static void main (String args[]) throws IOException {
        //Setting args
        int argsLength=args.length;
        Arguments arguments = new Arguments(argsLength,args[0],args[1]);
        arguments.arguments();

        ObjectInputStream read;
        System.out.println("Server started");
        Communication c = new Communication(arguments.portNumber);
        Socket clientSocket;
        clientSocket=c.accept();
        System.out.println("Server is using " + arguments.portNumber);

        read = new ObjectInputStream(clientSocket.getInputStream());

        //Using executor for threadpool
        ExecutorService executor = Executors.newFixedThreadPool(4);
        Runnable writeStudentRecordRQ = new WriteStudentRecordRQ(read,arguments.fileName);
        executor.execute(writeStudentRecordRQ);

        //executor.shutdown();
        //while (!executor.isTerminated()) {
        //}
    }
}