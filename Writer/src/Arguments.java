/**
 * Created by sumnima on 11/10/2016.
 */
public class Arguments {
    //When no args provided then set default values for constants
    public static final int PN=2500;
    public static final int MAXPN=3000;
    public static final String FN= "./resources/WriteStudentRecord.txt";
    int portNumber;
    String fileName;

    //args provided
    int argsLength;
    String arg1;
    String arg2;

    public Arguments(int argsLength, String arg1, String arg2) {
        this.argsLength=argsLength;
        this.arg1=arg1;
        this.arg2=arg2;
    }
    public void arguments() {
        if (argsLength == 0) {
            portNumber = PN;
            fileName = FN;
        } else {
            fileName = arg1;
            portNumber = Integer.parseInt(arg2);
        }
    }
}
