import java.io.*;

/**
 * Created by sumnima on 9/13/2016.
 */
public class WriteStudentRecordRQ implements Runnable{
    StudentRecord sr;
    String fN;
    ObjectInputStream objectInputStream;
    public WriteStudentRecordRQ( ObjectInputStream objectInputStream, String fileName)
    {
        this.objectInputStream =objectInputStream;
        this.fN=fileName;
    }
    @Override
    public void run()
    {
        File file = new File(fN);
        if (!file.exists()) {
            try {
                file.createNewFile();
                FileOutputStream fos = new FileOutputStream(file);
                BufferedWriter osw = new BufferedWriter(new OutputStreamWriter(fos));
                while(true) {
                    try {
                        sr = (StudentRecord) objectInputStream.readObject();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    System.out.print("\nSR"+sr.toString());
                    osw.write(sr.lastName);
                    osw.newLine();
                    osw.write(sr.firstName);
                    osw.newLine();
                    osw.write(sr.id);
                    osw.newLine();
                    osw.write(sr.dateOfBirth);
                    osw.newLine();
                    osw.write(Integer.toString(sr.ages));
                    osw.newLine();
                    if (sr.getId().equals("0")) {
                        osw.close();
                        break;
                    }
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
