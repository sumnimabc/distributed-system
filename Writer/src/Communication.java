import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by sumnima on 11/7/2016.
 */
public class Communication {
    Socket clientSocket = null;
    ServerSocket ss=null;
    int portNumber;
    Arguments arguments;

    public Communication(int portNumber) {
        this.portNumber=portNumber;
        while(true) {
            try {
                ss = new ServerSocket(portNumber);
                break;
                } catch (IOException e) {
                    portNumber = portNumber + 1;
                    if (portNumber == arguments.MAXPN) {
                        System.out.println("Port not available");
                        return;
                    }
                }
        }
    }

    public Socket accept()
    {
        try {
            clientSocket = ss.accept();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return clientSocket;
    }
}
